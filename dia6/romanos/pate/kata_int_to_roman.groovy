def redux_roman(num) {
  out = []

  map = [
    M: 1000, CM: 900, D: 500, CD: 400, C: 100, XC: 90,
    L: 50, XL: 40, X: 10, IX: 9, V: 5, IV: 4, I: 1
  ]

  map.each { key, value ->
    if (num >= value) {
       times = Math.floor(num / value)
       num -= times * value
       out << key * times
    }
  }

  out
}

def int_roman(num) {
  redux_roman(num).join()
}

assert int_roman(1999) == 'MCMXCIX'
assert int_roman(1024) == 'MXXIV'
assert int_roman(449) == 'CDXLIX'
assert int_roman(321) == 'CCCXXI'
assert int_roman(69) == 'LXIX'
assert int_roman(44) == 'XLIV'
assert int_roman(3) == 'III'
println int_roman(0)



