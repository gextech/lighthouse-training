//println 15.intdiv(5)

assert romanConvert(5) == 'V'
assert romanConvert(4) == 'IV'
assert romanConvert(100) == 'C'
assert romanConvert(500) == 'D'
assert romanConvert(2001) == 'MMI'


println romanConvert(5)
println romanConvert(2014)

def romanConvert(valx){
    romanval="";
    if (valx>=1000){
        half=valx.intdiv(1000)
        valx-=(half*1000)
        //println valx
        romanval+=romanNum(half,3)
    }
    if (valx>=100 && valx<1000){
        half=valx.intdiv(100)
        valx-=(half*100)
        romanval+=romanNum(half,2)
    }
    if (valx>10 && valx<100){
        half=valx.intdiv(10)
        valx-=(half*10)
        romanval+=romanNum(half,1)
    }
    if (valx<=10 && valx>0){
        half=valx
        romanval+=romanNum(half,0)
    }
    romanval
}
def romanNum(valx,posLit){
    literals = [['I','V','X'],['X','L','C'],['C','D','M']]
    romanlit=''
    if (posLit==3){
        1.upto(valx){
            romanlit+='M'
        }
    }else{
        if (valx<4){
            1.upto(valx){
               romanlit+=literals[posLit][0]
            }
        }
        if (valx==4){
            romanlit+=literals[posLit][0]+literals[posLit][1]
        }
        if (valx==9){
            romanlit+=literals[posLit][0]+literals[posLit][2]
        }
        if (valx>4 && valx<9){
            romanlit=literals[posLit][1]
            if (valx>5){
                6.upto(valx){
                   romanlit+=literals[posLit][0]
                }
            }
        }
        if (valx==10){
            romanlit+=literals[posLit][2]
        }

    }
    romanlit
}