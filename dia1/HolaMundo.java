import static java.lang.System.out;

public class HolaMundo extends Object {

  public static final java.lang.String ALGO = "algo";

  static class SuperClase {
    String getValue() {
      return "Abuela";
    }
  }

  static class Hija extends SuperClase {
    String getValue() {
      return "Hija";
    }
  }

  static class Nieta extends Hija {
    String getValue() {
      return "Nieta";
    }
  }

  private static String saludo;

  private static String foo() {
    return "foo";
  }
  public String getSaludo() {
    return saludo;
  }

  public static void main(String[] args) {

    int i = 1;
    Integer i2 = new Integer(1);

    Object o = i2;

    out.println(new String("Hola GEx"));

    HolaMundo.foo();

    HolaMundo hm = new HolaMundo();
    hm.getSaludo();



    SuperClase abuela = new SuperClase();
    Hija hija = new Hija();
    Nieta nieta = new Nieta();

    out.println(nieta.getValue());

    out.println(hija.getValue());

    if ( hija instanceof Nieta) {
      System.out.println(((Nieta) hija).getValue());
    } else {
      System.out.println("No es de ese tipo");
    }
    
    //System.out.println(((Object) hija).getValue());

    


  }
}