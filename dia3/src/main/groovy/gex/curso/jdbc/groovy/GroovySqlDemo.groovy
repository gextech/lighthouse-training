package gex.curso.jdbc

import groovy.sql.Sql

class GroovySqlDemo {
  void conecta() {
    def sql = Sql.newInstance("jdbc:h2:file:db.h2", "sa", "", "org.h2.Driver")
    def resultSet = sql.firstRow("SELECT count(id) as people_count from PEOPLE")

    println resultSet.people_count

    def people = sql.rows("SELECT * from people")

    people.each {person ->
      println "${person.id}, ${person.name_person}"
    }

    sql.close()
  }

  void foo() {
    println "Esto nunca se ejecuta"
  }
}