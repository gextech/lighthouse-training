package gex.curso.jdbc;

import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: domix
 * Date: 13/09/13
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
public class JdbcSamples {
  public Connection getConnection() {
    return connection;
  }

  private Connection connection;

  public void paso1() throws ClassNotFoundException {
    //Inicializar el driver de la base de datos con el DriverManager
    Class.forName("org.h2.Driver");
  }

  public Connection paso2() throws SQLException {
    Connection conn = DriverManager.getConnection("jdbc:h2:file:db.h2", "sa", "");
    return conn;
  }

  public void createTableIfNecessary() throws SQLException {
    Statement statement = connection.createStatement(); //DDL
    String createTable = "CREATE TABLE IF NOT EXISTS PEOPLE ( id INT, name_person varchar(255))";

    statement.execute(createTable);

    statement.close();
  }

  public boolean createPerson(Integer id, String name) throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO PEOPLE (id, name_person) values (?, ?)");
    preparedStatement.setInt(1, id);
    preparedStatement.setString(2, name);
    return preparedStatement.execute();
  }

  public Integer getCount() throws SQLException {
    PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(id) FROM PEOPLE");

    ResultSet resultSet = preparedStatement.executeQuery();
    resultSet.first();
    Integer peopleCount = resultSet.getInt(1);
    resultSet.close();

    preparedStatement.close();

    return peopleCount;
  }

  public void init() throws SQLException, ClassNotFoundException {
    paso1();
    this.connection = paso2();
    createTableIfNecessary();
  }

  public void close() throws SQLException {
    connection.close();
  }
}
