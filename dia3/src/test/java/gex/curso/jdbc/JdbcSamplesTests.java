package gex.curso.jdbc;

import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: domix
 * Date: 13/09/13
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 */
public class JdbcSamplesTests {
  @Test
  public void algo() throws SQLException, ClassNotFoundException {
    JdbcSamples jdbcSamples = new JdbcSamples();
    jdbcSamples.init();

    Integer peopleCount = jdbcSamples.getCount();

    jdbcSamples.createPerson(1, "GEx");
    Integer expected = peopleCount + 1;

    Assert.assertEquals(expected, jdbcSamples.getCount());

    jdbcSamples.close();
  }


}
