package gex.curso.jdbc;

/**
 * Created with IntelliJ IDEA.
 * User: domix
 * Date: 30/08/13
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */

import org.springframework.stereotype.Component;

@Component
public class OtroDaoConcreto implements  Dao {
  @Override
  public String obtenInformacion() {
    return "otro";
  }
}
