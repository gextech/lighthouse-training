package gex.curso.jdbc;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: domix
 * Date: 30/08/13
 * Time: 15:11
 * To change this template use File | Settings | File Templates.
 */
@Component
public class DaoConcreto implements Dao {

  public DaoConcreto() {
    System.out.println("Creando el DaoConcreto");
  }

  @Override
  public String obtenInformacion() {
    System.out.println("Estoy obteniendo algo");
    return "Mi dao";
  }

  public void algo() {

  }
}
