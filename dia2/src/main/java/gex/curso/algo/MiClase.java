package gex.curso.algo;

import gex.curso.jdbc.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class MiClase {
  //@Autowired
  private Dao dao;

  @Autowired
  public MiClase(@Qualifier("otroDaoConcreto")Dao dao) {
    System.out.println("Creando la clase MiClase");
    this.dao = dao;
  }

  public void doAlgo() {
    System.out.println(dao.obtenInformacion());
  }
}