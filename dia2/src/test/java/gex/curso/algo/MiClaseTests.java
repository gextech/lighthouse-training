package gex.curso.algo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextConfigurationAttributes;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created with IntelliJ IDEA.
 * User: domix
 * Date: 30/08/13
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/test.xml")
public class MiClaseTests {
  @Autowired
  MiClase lachida;
  @Test
  public void shouldDoSomething() {
    //MiClase miClase = new MiClase();
    //miClase.doAlgo();
    lachida.doAlgo();
  }
}
