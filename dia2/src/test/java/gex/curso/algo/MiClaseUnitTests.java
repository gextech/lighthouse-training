package gex.curso.algo;


import gex.curso.jdbc.Dao;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA.
 * User: domix
 * Date: 30/08/13
 * Time: 17:35
 * To change this template use File | Settings | File Templates.
 */
public class MiClaseUnitTests {
  @Test
  public void shouldSuccessBecauseYOLO() {
    Dao dao = mock(Dao.class);
    when(dao.obtenInformacion()).thenReturn("Hola");
    MiClase miClase = new MiClase(dao);
    miClase.doAlgo();
  }
  @Test(expected = RuntimeException.class)
  public void shouldFailBecauseRuntimeException() {
    Dao dao = mock(Dao.class);
    when(dao.obtenInformacion()).thenThrow(new RuntimeException());
    MiClase miClase = new MiClase(dao);
    miClase.doAlgo();
  }
}
