def solucion10 = [3, 5, 6, 9]
def multiplos = findMultiplos(9)

assert solucion10 == multiplos
assert 23 == multiplos.sum()
assert [3, 5, 6, 9, 10, 12, 15] == findMultiplos(15)
assert 233168 == findMultiplos(999).sum()

List findMultiplos(int tope) {
  (1..tope).findAll {num ->
    (num % 3) == 0 || (num % 5) == 0
  }
}
