def cacheFibonacci = [:]

assert 0 == fibonacci(0)
assert 1 == fibonacci(1)
assert 1 == fibonacci(2)
assert 377 == fibonacci(14)
assert 6765 == fibonacci(20)

def secuencia = []
def elemento = 0
def valor

def start = System.currentTimeMillis()
while(valor <= 4000000) {
   valor = fibonacci(elemento)
   elemento++
   if(valor <= 4000000) {
    secuencia << valor
   }
}
def end = System.currentTimeMillis()

println secuencia
println ((end - start)/1000)

println secuencia.findAll {
  it % 2 == 0
}.sum()


def fibonacci(int n) {
  def result
  if(n ==  0) {
    result = 0
  } else  if(n == 1) {
    result = 1
  } else {
    result = fibonacci(n-1)+fibonacci(n-2)
  }

  result
}




fi = {n ->
  def result
  if(n ==  0) {
    result = 0
  } else  if(n == 1) {
    result = 1
  } else {
    result = fi(n-1)+fi(n-2)
  }

  result
}

fiMem = fi.memoize()

assert 0 == fiMem(0)
assert 1 == fiMem(1)
assert 1 == fiMem(2)
assert 377 == fiMem(14)
assert 6765 == fiMem(20)




elemento = 0
secuencia = []
valor = 0
start = System.currentTimeMillis()
while(valor <= 4000000) {
   valor = fiMem(elemento)
   elemento++
   if(valor <= 4000000) {
    secuencia << valor
   }
}
end = System.currentTimeMillis()

println secuencia
println ((end - start)/1000)

start = System.currentTimeMillis()
println fiMem(32)
end = System.currentTimeMillis()
println ((end - start)/1000)