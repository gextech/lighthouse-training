List primes = [2, 3]
 
def factor = { value ->
  if (value in primes) return [value]
  for (long i = 2; i < value; i++) {
    if (value % i == 0)  return [i, (value / i) as long]
  }
  primes << value
  [value] 
} 

def primeFactors = { factors ->
  while ( factors.find { factor(it).size() > 1 } ) {
    factors = factors.collect({ factor it }).flatten()
  }
  factors
}
 
def sol = primeFactors([600851475143])

println sol
println sol.max()